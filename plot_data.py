'''
File Created: Friday, 14th February 2020 6:10:31 pm
Author: matteo (you@you.you)
-----
Last Modified: Friday, 14th February 2020 6:10:44 pm
Modified By: matteo (you@you.you>)
-----
Copyright 2017 - 2020 Your Company, Your Company
'''

# import pandas as pd
import os
import plotly.express as px

from read_files import (
    catch_invoices,
    build_plots
)

rpath = os.path.join(os.path.dirname(__file__), 'buy')
ipath = os.path.join(os.path.dirname(__file__), 'sell')

# grab the data from bot incoming and sent invoices
df, dfa = catch_invoices(ipath)
dfp, dfpa = catch_invoices(rpath)

# build the plots

fig1, fig2 = build_plots(df, dfa, dfp, dfpa)
fig1.show()
fig2.show()
