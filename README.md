# Easy build plot of your invoices

``fatturepy`` is a python library that is able to convert ``SDI`` ``XML``
Italian standard invoices to ``pandas`` dataframes. The library uses
``plotly.express`` to build the final plots.

## Usage

Clone or download the repository and download your invoices from your provider
and put them into the ``buy`` and ``sell`` directories.

Just run ``python3 plot_data.py`` or ``python plot_data.py``. 

## Requirements

Tha package relies on 3 main packages:

* ``xml``
* ``pandas``
* ``plotly.express``