'''
File: read_files.py
Project: pyfatture
File Created: Friday, 14th February 2020 3:51:52 pm
Author: matteo (you@you.you)
-----
Last Modified: Friday, 14th February 2020 3:58:50 pm
Modified By: matteo (you@you.you>)
-----
Copyright 2017 - 2020 Your Company, Your Company
'''

from collections import defaultdict
from datetime import datetime
import os
import xml.etree.ElementTree as ET
import pandas as pd
import plotly.express as px


def catch_invoices(fpath: str):
    """
    Convert XML invoices into pandas dataframe objects

    Parameters
    ----------
    fpath : str
        Directory path of the XML files.

    Returns
    -------
    df
        pandas dataframe with Date, Value, Buyer, Seller columns
    dfa
        pandas dataframe monthly aggregated values of df
    """

    dd = {
        'Date': [],
        'Value': [],
        'Compratore': [],
        'Venditore': []
    }

    # build a list of all the XML files within the directory given as input
    flist = [os.path.abspath(os.path.join(fpath, p)) for p in os.listdir(fpath)]


    for f in flist:

        # get the tree and its root
        try:
            tree = ET.parse(f)
            root = tree.getroot()
        except:
            pass

        # find all XML tags
        datefatture = root.findall(
            "./FatturaElettronicaBody/DatiGenerali/DatiGeneraliDocumento/Data"
        )
        importifatture = root.findall(
            "./FatturaElettronicaBody/DatiGenerali/DatiGeneraliDocumento/ImportoTotaleDocumento"
        )
        compratore = root.findall(
            "./FatturaElettronicaHeader/CessionarioCommittente/DatiAnagrafici/Anagrafica/Denominazione"
        )
        venditore = root.findall(
            "./FatturaElettronicaHeader/CedentePrestatore/DatiAnagrafici/Anagrafica/Denominazione"
        )

        # loop on date, value, buyer, seller
        for i, d, c, cp in zip(importifatture, datefatture, compratore, venditore):

            # append all the text to the initial dictionary
            dd['Date'].append(d.text)
            dd['Value'].append(float(i.text))
            dd['Compratore'].append(c.text)
            dd['Venditore'].append(cp.text)

        # convert the dictionary in a pandas DataFrame
        df = pd.DataFrame(dd)

        # convert the Date column into a datetime index
        df.index = pd.to_datetime(df["Date"])

        # make a monthly sum of all the values
        dfa = df.resample("M")['Value'].agg(['sum'])

        
    return df, dfa


def build_plots(df: pd.DataFrame, dfa: pd.DataFrame, dfp: pd.DataFrame, dfpa: pd.DataFrame):
    """
    Takes dataframes objects and build plotly graphs of Sales VS Purchases

    Parameters
    ----------
    df : pd.DataFrame
        pandas dataframe of the sales obtained from the :func:`catch_invoices`
        function
    dfa : pd.DataFrame
        pandas aggregated dataframe of the sales obtained from the 
        :func:`catch_invoices` function
    df : pd.DataFrame
        pandas dataframe of the purchases obtained from the :func:`catch_invoices`
        function
    dfa : pd.DataFrame
        pandas aggregated dataframe of the purchases obtained from the 
        :func:`catch_invoices` function

    Returns
    -------
    fig1
        plotly figure object of Sales Vs Purchases.
    fig2
        plotly figure object of the aggregated monthly Sales Vs Purchases.
    """

    # fig1 - Sales
    fig1 = px.bar(df, x='Date', y='Value', hover_name="Compratore")
    fig1.data[0].name = 'Vendite'
    fig1.data[0].marker.color = '#39a728'
    fig1.data[0].showlegend = True

    # fig1 - Purchases
    fig1.add_trace(
        px.bar(dfp, "Date", "Value", hover_name="Venditore").data[0]
    )
    fig1.data[1].marker.color = '#f13a0d'
    fig1.data[1].name = 'Acquisti'
    fig1.data[1].showlegend = True

    # fig1 layout
    fig1.layout.title = 'Vendita Vs Acquisto'
    fig1.layout.xaxis.title.text = 'Data'
    fig1.layout.xaxis.tickformat = "%Y-%m-%d"
    fig1.layout.yaxis.title.text = '€'

    # fig2 - Sales
    fig2 = px.bar(dfa, x=dfa.index, y='sum')
    fig2.data[0].name = 'Vendite'
    fig2.data[0].marker.color = '#39a728'
    fig2.data[0].showlegend = True

    # fig2 - Purchases
    fig2.add_trace(
        px.bar(dfpa, x=dfpa.index, y='sum').data[0]
    )
    fig2.data[1].marker.color = '#f13a0d'
    fig2.data[1].name = 'Acquisti'
    fig2.data[1].showlegend = True

    # fig2- layout
    fig2.layout.title = 'Resampling Mensile'
    fig2.layout.xaxis.tickformat = "%Y-%m"
    fig2.layout.xaxis.title.text = 'Anno-Mese'
    fig2.layout.yaxis.title.text = '€'

    return fig1, fig2
